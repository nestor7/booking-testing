<?php

use Faker\Generator as Faker;

$factory->define(App\Client::class, function (Faker $faker) {
    return [
        'username' => $faker->userName,
    ];
});

$factory->define(App\Booking::class, function (Faker $faker) {
    return [
        'client_id' => function () {
            return factory(App\Client::class)->create()->id;
        },
        'visiting_reason' => $faker->paragraph
    ];
});
