<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/



/*
 *  The routes for BookingController
 *
 *  Verb          Path                        Action
 *
 *  GET           /booking                    index
 *  POST          /booking                    store
 *  GET           /booking/{booking_id}       show
 *  PUT|PATCH     /booking/{booking_id}       update
 *  DELETE        /booking/{booking_id}       destroy
 *
 */
Route::resource(
    '/booking',
    'BookingController',
    [
        'names' => [
            'index'   => 'booking.get',
            'update'  => 'booking.patch',
            'store'   => 'booking.post',
            'destroy' => 'booking.delete',
        ],
    ]
);
