<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Faker\Factory as Faker;
use App\BookingRepository;
use App\Booking;

class BookingRepositoryTest extends TestCase
{
    protected $fake;

    public function setUp()
    {
        parent::setUp();

        $this->fake = Faker::create();
    }

    /**
     * Create a booking with the repository
     * If the username not exists as a client it will create a new one
     *
     * @test
     */
    public function itCanCreateABooking()
    {
        $data = [
            'username' => $this->fake->userName,
            'visiting_reason' => $this->fake->paragraph,
        ];

        $repo = new BookingRepository(new Booking);
        $booking = $repo->createBooking(new Request($data));

        $this->assertInstanceOf(Booking::class, $booking);
        $this->assertEquals($data['username'], $booking->client->username);
        $this->assertEquals($data['visiting_reason'], $booking->visiting_reason);
    }

    /**
     * Update a booking with the repository
     *
     * @test
     */
    public function itCanUpdateTheBooking()
    {
        $booking = factory(Booking::class)->create();

        $data = [
            'visiting_reason' => $this->fake->paragraph
        ];

        $repo = new BookingRepository($booking);
        $update = $repo->updateBooking($data);

        $this->assertTrue($update);
        $this->assertEquals($data['visiting_reason'], $booking->visiting_reason);
    }

    /**
     * Find a booking with the repository
     *
     * @test
     */
    public function itCanShowTheBooking()
    {
        $booking = factory(Booking::class)->create();

        $repo = new BookingRepository(new Booking());

        $found = $repo->findBooking($booking->id);

        $this->assertInstanceOf(Booking::class, $found);
        $this->assertEquals($found->client_id, $booking->client->id);
        $this->assertEquals($found->visiting_reason, $booking->visiting_reason);
    }

    /**
     * Delete a booking with the repository
     *
     * @test
     */
    public function itCanDeleteTheBooking()
    {
        $booking = factory(Booking::class)->create();

        $repo = new BookingRepository($booking);

        $delete = $repo->deleteBooking();

        $this->assertTrue($delete);
    }

}
