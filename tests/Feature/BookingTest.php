<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Faker\Factory as Faker;
use App\BookingRepository;
use App\Booking;

class BookingTest extends TestCase
{
  //  use RefreshDatabase;

    protected $fake;

    public function setUp()
    {
        parent::setUp();

        $this->fake = Faker::create();
    }

    /**
     * Call api to get the booking by Request
     *
     * @test
     */
    public function apiToGetBookingByID()
    {
        // create a new booking
        $booking = factory('App\Booking')->create();

        // make the request to get the same booking
        $response = $this->getJson(
            '/api/booking/' . $booking->id
        );

        // check if the request was successfully
        $this->assertEquals(200, $response->getStatusCode());

        //compare the ids if they are the same
        $this->assertEquals($booking->id, $response->getData()->id);
    }

    /**
     * Call api to get all bookings by Request
     *
     * @test
     */
    public function apiToGetAllBookings()
    {
        // make the request to get all bookings
        $response = $this->getJson(
            '/api/booking/'
        );

        // check if the request was successfully
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * Call api to create a booking by Request
     * If the username not exists as a client it will create a new one
     *
     * @test
     */
    public function apiToCreateBooking()
    {
        // send the request to create a new booking
        $response = $this->postJson(
            '/api/booking',
            [
                'username' =>  $this->fake->userName,
                'visiting_reason' => $this->fake->paragraph,
            ]
        );

        // check if the request was successfully
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * Call api to create a booking with an empty request
     * this should never happen
     *
     * @test
     */
    public function createBookingWithEmptyRequest()
    {
        // send the request to create a booking with empty values
        $response = $this->postJson(
            '/api/booking',
            []
        );
        $this->assertEquals(422, $response->getStatusCode());
    }

    /**
     * Call api to delete a booking by Request
     *
     * @test
     */
    public function apiToDeleteBooking()
    {
        // create a new booking
        $booking = factory('App\Booking')->create();

        //delete the booking
        $response = $this->call(
            'DELETE',
            '/api/booking/' . $booking->id
        );

        // check if it was successfully
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * Call api to update a booking by Request
     *
     * @test
     */
    public function apiToUpdateBooking()
    {
        // create a new booking
        $booking = factory('App\Booking')->create();

        //create new visiting reason
        $newReason = $this->fake->paragraph;

        // update the visiting reason on booking
        $response = $this->call(
            'PATCH',
            '/api/booking/'. $booking->id,
            [
                'visiting_reason' => $newReason,
            ]
        );

        // check if it was successfully
        $this->assertEquals(200, $response->getStatusCode());
    }
}
