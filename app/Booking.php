<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = [
        'client_id',
        'visiting_reason'
    ];

    /**
     * Get the client that owns the Booking.
     */
    public function client()
    {
        return $this->belongsTo('App\Client');
    }

}
