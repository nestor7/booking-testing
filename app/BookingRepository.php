<?php

namespace App;

use App\Booking;
use App\Client;
use App\Exceptions\BookingCreateException;
use App\Exceptions\BookingUpdateException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class BookingRepository
{
    /**
     * @var \App\Booking
     */
    protected $model;

    /**
     * BookingRepository constructor.
     * @param \App\Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->model = $booking;
    }

    /**
     * Get all instances of model
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * @param Request $request
     * @return \App\Booking
     * @throws BookingCreateException
     */
    public function createBooking(Request $request) : Booking
    {
        try {
            // validate the data before storing
            $data = $request->validate([
                'username' => 'required',
                'visiting_reason' => 'required',
            ]);

            //get the client
            $client = Client::where('username', '=', $data['username'])->first();

            // if no client, create a new one
            if ($client === null) {
                $client = Client::create([
                    'username' => $data['username']
                ]);
            }

            return $this->model->create([
                'client_id' => $client->id,
                'visiting_reason' => $data['visiting_reason']
            ]);
        } catch (QueryException $e) {
            throw new BookingCreateException($e);
        }
    }

    /**
     * @param int $id
     * @return \App\Booking
     */
    public function findBooking(int $id) : Booking
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new BookingNotFoundException($e);
        }
    }

    public function updateBooking(array $data) : bool
    {
        try {
            return $this->model->update($data);
        } catch (QueryException $e) {
            throw new BookingUpdateException($e);
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteBooking() : bool
    {
        return $this->model->delete();
    }

}