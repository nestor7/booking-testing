<?php

namespace App\Exceptions;

use Illuminate\Http\Response;

class BookingCreateException extends \Exception
{
    /**
     * @var string
     */
    protected $message = "Booking cannot be created";

    /**
     * @var int
     */
    protected $code = Response::HTTP_BAD_REQUEST;
}