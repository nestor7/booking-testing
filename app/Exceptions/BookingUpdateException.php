<?php

namespace App\Exceptions;

use Illuminate\Http\Response;

class BookingUpdateException extends \Exception
{
    /**
     * @var string
     */
    protected $message = "Booking cannot be updated";

    /**
     * @var int
     */
    protected $code = Response::HTTP_BAD_REQUEST;
}