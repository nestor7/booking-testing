<?php

namespace App\Exceptions;

use Illuminate\Http\Response;

class BookingNotFoundException extends \Exception
{
    /**
     * @var string
     */
    protected $message = "Booking cannot be found";

    /**
     * @var int
     */
    protected $code = Response::HTTP_NOT_FOUND;
}