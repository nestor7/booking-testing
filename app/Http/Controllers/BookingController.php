<?php

namespace App\Http\Controllers;

use App\Booking;
use App\BookingRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BookingController extends Controller
{
    /**
     * @var BookingRepository
     */
    protected $repository;

    /**
     * BookingController constructor.
     *
     * @param Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->repository = new BookingRepository($booking);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        //return all bookings
        return $this->repository->all();
    }

    /**
     * Store a newly created resource in storage.
     * Create a new client if the username doesn't exists as a client
     *
     * @param Request $request
     * @return Booking
     * @throws \App\Exceptions\BookingCreateException
     */
    public function store(Request $request)
    {
        return $this->repository->createBooking($request);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return Booking
     */
    public function show($id)
    {
        return $this->repository->findBooking($id);
    }

    /**
     * Update only the visiting reason.
     *
     * @param Request $request
     * @param Booking $booking
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\BookingUpdateException
     */
    public function update(Request $request, Booking $booking)
    {
        $repo = new BookingRepository($booking);
        return response()->json($repo->updateBooking($request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        $repo = new BookingRepository($booking);
        return response()->json($repo->deleteBooking());
    }
}
